### Zerotier

##### Network config

At this point, if a ZT network was provided, node should be present there:

![](../images/zt_master_node.png "Master node as shown in zerotier")

Note that, if the ZT network is private (recommended), node must be authorized so it can join the network and obtain an IP.

Other devices connected to this zerotier network must be able to reach the private IP of the k8s master node. For this, add a route to your master node that says: k8s private IP/32 via k8s master node's ZT IP:

![](../images/zt_routes.png "Route to reach k8s node")

All services use an ingress object. To access them from the ZT network, a new DNS server must be forced into the devices that are part of the ZT network. Add k8s master IP (this is the IP used by the LB object that routes traffic to adguard) as a DNS server:

![](../images/zt_dns.png "DNS servers in ZT network")

##### Android Client

Install Zerotier in Android. Add the network and make sure to set **Network DNS** setting.
