# asdasd


##

Format a flash drive in ext3, set a label for the partition called **optwate**.

In the router, go to Administration » Scripts » Init and add the following line, then click [SAVE]:

```
echo "LABEL=optware /opt ext3 defaults 1 1" >> /etc/fstab
```

Also run that via ssh.

Connect flash drive and install entware.

## Install entware

Flash drive must be mounted already in */opt/*.

```
cd /opt
wget http://qnapware.zyxmon.org/binaries-armv7/installer/entware_install_arm.sh
chmod +x entware_install_arm.sh
./entware_install_arm.sh
```

## Install nfs server

```
opkg install nfs-kernel-server
```

## Mount HDD


In the router, go to Administration » Scripts » Init and add the following line, then click [SAVE]:

```
echo "UUID=1abe4d63-36d0-4d57-afc6-ea896cd312e9 /mnt ext4 defaults 0 0" >> /etc/fstab
```

Also add it manually to fstab or reboot.

Connect hdd.

## Configure export

mkdir -p /opt/var/lib/nfs/
