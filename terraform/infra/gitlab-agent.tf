resource "helm_release" "gitlab_agent" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_agent_token"] == "" ? 0 : 1
  name       = "gitlab-agent"
  chart      = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  namespace  = "default"
  version    = var.versions.gitlab_agent.chart
  timeout    = "300"

  values = [<<EOF
replicas: 1
config:
  kasAddress: wss://kas.gitlab.com
  token: ${data.sops_file.secrets.data["gitlab_kubernetes_agent_token"]}
resources:
  limits:
   cpu: 800m
   memory: 200Mi
  requests:
   cpu: 200m
   memory: 100Mi
EOF
  ]
}

resource "helm_release" "gitlab_kraken_bot_agent" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_kraken_agent_token"] == "" ? 0 : 1
  name       = "gitlab-kraken-bot-agent"
  chart      = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  namespace  = "default"
  version    = var.versions.gitlab_agent.chart
  timeout    = "300"

  values = [<<EOF
replicas: 1
config:
  kasAddress: wss://kas.gitlab.com
  token: ${data.sops_file.secrets.data["gitlab_kubernetes_kraken_agent_token"]}
resources:
  limits:
   cpu: 600m
   memory: 200Mi
  requests:
   cpu: 50m
   memory: 50Mi
EOF
  ]
}

resource "helm_release" "gitlab_smart_hotels_bot_agent" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_kraken_agent_token"] == "" ? 0 : 1
  name       = "gitlab-smart-hotels-bot-agent"
  chart      = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  namespace  = "default"
  version    = var.versions.gitlab_agent.chart
  timeout    = "300"

  values = [<<EOF
replicas: 1
config:
  kasAddress: wss://kas.gitlab.com
  token: ${data.sops_file.secrets.data["gitlab_kubernetes_smart_hotels_agent_token"]}
resources:
  limits:
   cpu: 600m
   memory: 200Mi
  requests:
   cpu: 50m
   memory: 50Mi
EOF
  ]
}

resource "helm_release" "gitlab_smart_hotels_infra_bot_agent" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_kraken_agent_token"] == "" ? 0 : 1
  name       = "gitlab-smart-hotels-infra-bot-agent"
  chart      = "gitlab-agent"
  repository = "https://charts.gitlab.io"
  namespace  = "default"
  version    = var.versions.gitlab_agent.chart
  timeout    = "300"

  values = [<<EOF
replicas: 1
config:
  kasAddress: wss://kas.gitlab.com
  token: ${data.sops_file.secrets.data["gitlab_kubernetes_smart_hotels_infra_agent_token"]}
resources:
  limits:
   cpu: 600m
   memory: 200Mi
  requests:
   cpu: 50m
   memory: 50Mi
EOF
  ]
}
