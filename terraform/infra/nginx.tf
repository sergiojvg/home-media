resource "helm_release" "nginx" {
  count      = data.sops_file.secrets.data["gitlab_kubernetes_agent_token"] == "" ? 0 : 1
  name       = "ingress-nginx"
  chart      = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  namespace  = "default"
  version    = var.versions.nginx.chart
  #version    = "4.12.0"
  timeout = "100"

  values = [<<EOF
controller:
  #healthStatus: true
  #healthStatusURI: "/ping"
  #enableLatencyMetrics: true
  #stats:
  #  enabled: true
  #metrics:
  #  enabled: true
  #  service:
  #    annotations:
  #      prometheus.io/scrape: "true"
  #      prometheus.io/port: "10254"
  #ingressClass:
  #  name: nginx
  #  create: true
  #  setAsDefaultIngress: true
  resources:
    requests:
      cpu: 150m
      memory: 128Mi
    limits:
      cpu: 400m
      memory: 800Mi
  service:
    loadBalancerIP: 192.168.1.221
    #type: NodePort
    type: LoadBalancer
    #externalTrafficPolicy: Cluster ### Local preserves the IP of the client, but nginx would have to be ran as a daemonset 
    ports:
      http: 30080
      https: 30443
    nodeports:
      http: 30080
      https: 30443
    #httpPort:
    #  nodePort: 30080
    #httpsPort:
    #  nodePort: 30443
    #annotations: 
    #  metallb.universe.tf/loadBalancerIPs: ${var.ingress_ip}
#prometheus:
#  create: true
#  port: 9113
#  scheme: http
EOF
  ]
  depends_on = [
    kubernetes_manifest.reserved_address_pool
  ]
}
