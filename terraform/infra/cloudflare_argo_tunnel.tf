resource "random_id" "argo_secret" {
  byte_length = 35
}

resource "cloudflare_tunnel" "ben" {
  account_id = data.cloudflare_zone.vegabookholt.0.account_id
  name       = "ben"
  secret     = random_id.argo_secret.b64_std
}

resource "cloudflare_access_group" "home_emails" {
  account_id = data.cloudflare_zone.vegabookholt.0.account_id
  name       = "home-emails"

  include {
    email = ["sergiojvg92@gmail.com", "inge.bookholt@gmail.com", "jramirezg.1944@gmail.com"]
  }
}

resource "cloudflare_access_identity_provider" "google_oauth" {
  account_id = data.cloudflare_zone.vegabookholt.0.account_id
  name       = "Google OAuth"
  type       = "google"
  config {
    client_id     = data.sops_file.secrets.data["google_cloudflare_oauth_client_id"]
    client_secret = data.sops_file.secrets.data["google_cloudflare_oauth_client_secret"]
  }
}

resource "kubernetes_secret" "tunnel_credentials" {
  metadata {
    name = "tunnel-credentials"
  }

  data = {
    "credentials.json" = jsonencode({
      AccountTag   = data.cloudflare_zone.vegabookholt.0.account_id
      TunnelSecret = cloudflare_tunnel.ben.secret
      TunnelID     = cloudflare_tunnel.ben.id
    })
  }
}

resource "kubernetes_config_map_v1" "cloudflared_config" {
  metadata {
    name = "cloudflared-config"
  }

  data = {
    "config.yaml" = <<EOT
# Name of the tunnel you want to run
tunnel: ${cloudflare_tunnel.ben.name}
credentials-file: /etc/cloudflared/creds/credentials.json
# Serves the metrics server under /metrics and the readiness server under /ready
metrics: 0.0.0.0:2000
# Autoupdates applied in a k8s pod will be lost when the pod is removed or restarted, so
# autoupdate doesn't make sense in Kubernetes. However, outside of Kubernetes, we strongly
# recommend using autoupdate.
no-autoupdate: true
# The `ingress` block tells cloudflared which local service to route incoming
# requests to. For more about ingress rules, see
# https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/configuration/ingress
#
# Remember, these rules route traffic from cloudflared to a local service. To route traffic
# from the internet to cloudflared, run `cloudflared tunnel route dns <tunnel> <hostname>`.
# E.g. `cloudflared tunnel route dns example-tunnel tunnel.example.com`.
ingress:
# The first rule proxies traffic to the httpbin sample Service defined in app.yaml
%{for tunnel in var.cloudflare_app_tunnels}
- hostname: ${tunnel.hostname}.${var.public_domain}
  service: ${tunnel.kubernetes_service_url} %{endfor}
# This rule sends traffic to the built-in hello-world HTTP server. This can help debug connectivity
# issues. If hello.example.com resolves and tunnel.example.com does not, then the problem is
# in the connection from cloudflared to your local service, not from the internet to cloudflared.
- hostname: tunnel-test.${var.public_domain}
  service: hello_world
# This rule matches any traffic which didn't match a previous rule, and responds with HTTP 404.
- service: http_status:404
EOT
  }
}

resource "kubernetes_deployment" "cloudflared" {
  metadata {
    name = "cloudflared"
    labels = {
      app = "cloudflared"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "cloudflared"
      }
    }

    template {
      metadata {
        labels = {
          app = "cloudflared"
        }
      }

      spec {
        container {
          image = "cloudflare/cloudflared:${var.versions.cloudflared.image}"
          name  = "cloudflared"
          args  = ["tunnel", "--config", "/etc/cloudflared/config/config.yaml", "run"]

          resources {
            limits = {
              cpu    = "0.3"
              memory = "80Mi"
            }
            requests = {
              cpu    = "150m"
              memory = "60Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/ready"
              port = 2000

            }

            initial_delay_seconds = 10
            period_seconds        = 10
          }
          volume_mount {
            mount_path = "/etc/cloudflared/config"
            name       = "config"
          }
          volume_mount {
            mount_path = "/etc/cloudflared/creds"
            name       = "creds"
          }
        }
        volume {
          name = "creds"
          secret {
            secret_name = kubernetes_secret.tunnel_credentials.metadata.0.name
          }
        }
        volume {
          name = "config"
          config_map {
            name = kubernetes_config_map_v1.cloudflared_config.metadata.0.name
            items {
              key  = "config.yaml"
              path = "config.yaml"
            }
          }
        }
      }
    }
  }
}

resource "cloudflare_record" "homer_argo_tunnel" {
  count   = length(var.cloudflare_app_tunnels)
  zone_id = data.cloudflare_zone.vegabookholt.0.id
  name    = var.cloudflare_app_tunnels[count.index].hostname
  value   = "${cloudflare_tunnel.ben.id}.cfargotunnel.com"
  type    = "CNAME"
  proxied = true
  ttl     = 1
}

resource "cloudflare_access_application" "access_application" {
  count            = length(var.cloudflare_app_tunnels)
  zone_id          = data.cloudflare_zone.vegabookholt.0.id
  name             = var.cloudflare_app_tunnels[count.index].hostname
  domain           = "${var.cloudflare_app_tunnels[count.index].hostname}.vegabookholt.uk"
  type             = "self_hosted"
  session_duration = "24h"
  #auto_redirect_to_identity = false
}

resource "cloudflare_access_policy" "access_policy" {
  count          = length(var.cloudflare_app_tunnels)
  application_id = cloudflare_access_application.access_application[count.index].id
  zone_id        = data.cloudflare_zone.vegabookholt.0.id
  name           = var.cloudflare_app_tunnels[count.index].hostname
  precedence     = "1"
  decision       = "allow"

  include {
    email = ["sergiojvg92@gmail.com", "inge.bookholt@gmail.com", "jramirezg.1944@gmail.com"]
  }
}
