#########################################################################
############################## CERTMANAGER ##############################
#########################################################################

resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  namespace  = "default"
  timeout    = "300"
  version    = var.versions.certmanager.chart

  values = [<<EOF
installCRDs: true
EOF
  ]
}

#resource "kubernetes_manifest" "clusterissuer" {
#  manifest = {
#    "apiVersion" = "cert-manager.io/v1"
#    "kind"       = "ClusterIssuer"
#    "metadata" = {
#      "name" = "letsencrypt"
#    }
#    "spec" = {
#      "acme" = {
#        "server" = "https://acme-v02.api.letsencrypt.org/directory"
#        #"server"              = "https://acme-staging-v02.api.letsencrypt.org/directory"
#        "email" = var.personal_email
#        "privateKeySecretRef" = {
#          "name" = "issuer-account-key"
#        }
#        "solvers" = [{
#          "http01" = {
#            "ingress" = {
#              "class" = "traefik"
#            }
#          }
#        }]
#      }
#    }
#  }
#}

resource "kubernetes_manifest" "dns_clusterissuer" {
  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-dns"
    }
    "spec" = {
      "acme" = {
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        #"server"              = "https://acme-staging-v02.api.letsencrypt.org/directory"
        "email" = var.personal_email
        "privateKeySecretRef" = {
          "name" = "dns-issuer-account-key"
        }
        "solvers" = [{
          "dns01" = {
            "cloudflare" = {
              "apiTokenSecretRef" = {
                "name" = "cloudflare-token-letsencrypt"
                "key"  = "api-key"
              }
            }
          }
        }]
      }
    }
  }
}


#resource "kubernetes_manifest" "ha_certificate" {
#  manifest = {
#    "apiVersion" = "cert-manager.io/v1"
#    "kind"       = "Certificate"
#    "metadata" = {
#      "name"      = "homeassistant"
#      "namespace" = "default"
#    }
#    "spec" = {
#      "dnsNames" = [
#        "ha-my-house.duckdns.org"
#      ]
#      "secretName" = "ha-my-house.duckdns.org-tls"
#      "issuerRef" = {
#        "name" = kubernetes_manifest.clusterissuer.manifest.metadata.name
#        "kind" = kubernetes_manifest.clusterissuer.manifest.kind
#      }
#    }
#  }
#}

resource "kubernetes_manifest" "wildcard_certificate" {
  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "Certificate"
    "metadata" = {
      "name"      = "wildcard-certificate"
      "namespace" = "default"
    }
    "spec" = {
      "dnsNames" = [
        "*.${var.public_domain}"
      ]
      "commonName" = "*.${var.public_domain}"
      "secretName" = "wildcard-certificate"
      "issuerRef" = {
        "name" = kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
        "kind" = kubernetes_manifest.dns_clusterissuer.manifest.kind
      }
    }
  }
}

#resource "kubernetes_manifest" "traefik_redirect_middleware" {
#  manifest = {
#    "apiVersion" = "traefik.containo.us/v1alpha1"
#    "kind"       = "Middleware"
#    "metadata" = {
#      "name"      = "redirect"
#      "namespace" = "default"
#    }
#    "spec" = {
#      "redirectScheme" = {
#        "scheme"    = "https"
#        "permanent" = true
#      }
#    }
#  }
#}

#resource "kubernetes_manifest" "ha_ingress_route" {
#  manifest = {
#    apiVersion = "traefik.containo.us/v1alpha1"
#    kind       = "IngressRoute"
#    metadata = {
#      name      = "homeassistant"
#      namespace = "default"
#    }
#    spec = {
#      entryPoints = [
#        "web",
#        "websecure"
#      ]
#      routes = [{
#        #match = "Host(`ha-my-house.duckdns.org`)"
#        match = "Host(`homeassistant.${var.public_domain}`)"
#        kind  = "Rule"
#        services = [{
#          name = "homeassistant-home-assistant"
#          port = 80
#        }]
#      }]
#      tls = {
#        #secretName = kubernetes_manifest.ha_certificate.manifest.spec.secretName
#        secretName = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
#      }
#    }
#  }
#  field_manager {
#    force_conflicts = true
#  }
#}

#resource "kubernetes_manifest" "plex_ingress_route" {
#  manifest = {
#    apiVersion = "traefik.containo.us/v1alpha1"
#    kind       = "IngressRoute"
#    metadata = {
#      name      = "plex"
#      namespace = "default"
#    }
#    spec = {
#      entryPoints = [
#        "web",
#        "websecure"
#      ]
#      routes = [{
#        match = "Host(`plex.${var.public_domain}`)"
#        kind  = "Rule"
#        services = [{
#          name = "plex"
#          port = 80
#        }]
#      }]
#      tls = {
#        secretName = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
#      }
#    }
#  }
#  field_manager {
#    force_conflicts = true
#  }
#}
#
#resource "kubernetes_manifest" "overseerr_ingress_route" {
#  manifest = {
#    apiVersion = "traefik.containo.us/v1alpha1"
#    kind       = "IngressRoute"
#    metadata = {
#      name      = "overseerr"
#      namespace = "default"
#    }
#    spec = {
#      entryPoints = [
#        "web",
#        "websecure"
#      ]
#      routes = [{
#        match = "Host(`overseerr.${var.public_domain}`)"
#        kind  = "Rule"
#        services = [{
#          name = "overseerr"
#          port = 80
#        }]
#      }]
#      tls = {
#        secretName = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
#      }
#    }
#  }
#  field_manager {
#    force_conflicts = true
#  }
#}
