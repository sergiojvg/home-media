versions = {
  grafana = {
    chart = "8.8.5" ### grafana/grafana
  }
  prometheus = {
    chart = "27.1.0" ### prometheus-community/prometheus
  }
  prometheus_blackbox_exporter = {
    chart = "9.1.0" ### prometheus-community/prometheus-blackbox-exporter
  }
  otel_collector = {
    chart = "0.115.0" ### open-telemetry/opentelemetry-collector
  }
  node_problem_detector = {
    chart = "2.3.14" ### deliveryhero/node-problem-detector
  }
}
