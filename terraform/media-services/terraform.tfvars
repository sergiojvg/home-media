versions = {
  radarr = {
    chart = "16.3.2"
    image = "5.17.2" ### https://fleet.linuxserver.io/image?name=linuxserver/radarr  ### https://hub.docker.com/r/linuxserver/radarr/tags
  }
  sonarr = {
    chart = "16.3.2"
    image = "4.0.11" ### https://fleet.linuxserver.io/image?name=linuxserver/sonarr ### https://hub.docker.com/r/linuxserver/sonarr/tags
  }
  jellyfin = {
    chart = "9.5.3"
    image = "10.10.4" ### https://fleet.linuxserver.io/image?name=linuxserver/jellyfin ### https://hub.docker.com/r/linuxserver/jellyfin/tags
  }
  overseerr = {
    chart = "5.4.2"
    image = "1.33.2" ### https://github.com/sct/overseerr/releases
  }
  prowlarr = {
    chart = "4.5.2"
    image = "1.30.2" ### https://fleet.linuxserver.io/image?name=linuxserver/prowlarr ### https://hub.docker.com/r/linuxserver/prowlarr/tags
  }
  bazarr = {
    chart = "10.6.2"
    image = "1.5.1" ### https://fleet.linuxserver.io/image?name=linuxserver/bazarr ### https://hub.docker.com/r/linuxserver/bazarr/tags
  }
  transmission = {
    image = "4.0.6" ### https://hub.docker.com/r/linuxserver/transmission/tags
  }
  mealie = {
    image = "v2.4.1" ### https://github.com/mealie-recipes/mealie/tags ### https://hub.docker.com/r/hkotel/mealie/tags
  }
}
