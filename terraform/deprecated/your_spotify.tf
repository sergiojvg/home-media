##########################################################################
################################## YOUR_SPOTIFY ##########################
##########################################################################
#
#resource "kubernetes_deployment_v1" "your_spotify" {
#  metadata {
#    name = "your-spotify"
#    labels = {
#      app = "your-spotify"
#    }
#  }
#  spec {
#    replicas = 1
#    selector {
#      match_labels = {
#        app = "your-spotify"
#      }
#    }
#    template {
#      metadata {
#        labels = {
#          app = "your-spotify"
#        }
#      }
#      spec {
#        container {
#          image   = "linuxserver/your_spotify:1.6.0"
#          name    = "your-spotify"
#          #command = ["/bin/bash", "-c"]
#          #args    = ["cp /temp-config/settings.json /config/settings.json && /init"]
#
#          resources {
#            limits = {
#              cpu    = "800m"
#              memory = "756Mi"
#            }
#            requests = {
#              cpu    = "200m"
#              memory = "256Mi"
#            }
#          }
#          liveness_probe {
#            tcp_socket {
#              port = 9091
#            }
#          }
#          readiness_probe {
#            tcp_socket {
#              port = 9091
#            }
#          }
#          volume_mount {
#            mount_path = "/temp-config"
#            name       = "transmission-config"
#          }
#          volume_mount {
#            mount_path = "/downloads"
#            name       = "downloads"
#          }
#        }
#        volume {
#          name = "transmission-config"
#          config_map {
#            name         = kubernetes_config_map_v1.transmission_config.metadata.0.name
#            default_mode = "0777"
#          }
#        }
#        volume {
#          name = "downloads"
#          persistent_volume_claim {
#            claim_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc
#          }
#        }
#      }
#    }
#  }
#}
#
#resource "kubernetes_service" "transmission" {
#  metadata {
#    name = "transmission"
#  }
#  spec {
#    selector = {
#      app = "transmission"
#    }
#    port {
#      port        = 9091
#      target_port = 9091
#    }
#  }
#  timeouts {
#    create = "1m"
#  }
#}
#
