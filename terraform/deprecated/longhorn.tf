#resource "kubernetes_namespace" "longhorn" {
#  metadata {
#    name = "longhorn-system"
#  }
#}
#
##resource "kubernetes_limit_range" "longhorn" {
##  metadata {
##    name      = "default-resources"
##    namespace = kubernetes_namespace.longhorn.metadata.0.name
##  }
##  spec {
##    limit {
##      type = "Container"
##      default_request = {
##        cpu    = "200m"
##        memory = "200Mi"
##      }
##      default = {
##        cpu    = "400m"
##        memory = "350Mi"
##      }
##    }
##  }
##}
#
#resource "helm_release" "longhorn" {
#  name      = "longhorn"
#  chart     = "../../longhorn/chart"
#  namespace = kubernetes_namespace.longhorn.metadata.0.name
#  timeout   = "1200"
#
#  values = [<<EOF
#longhornManager:
#  priorityClass: ${kubernetes_priority_class_v1.important.metadata.0.name} 
#longhornDriver:
#  priorityClass: ${kubernetes_priority_class_v1.important.metadata.0.name} 
#persistence:
#  defaultClassReplicaCount: 3
#  defaultDataLocality: best-effort
#ingress:
#  enabled: true
#  host: longhorn.${var.local_domain}
#  annotations: 
#    kubernetes.io/ingress.class: nginx
#defaultSettings:
#  defaultReplicaCount: 3
#  defaultDataLocality: best-effort
#  storageMinimalAvailablePercentage: 5
#  storageOverProvisioningPercentage: 125
#  priorityClass: ${kubernetes_priority_class_v1.important.metadata.0.name} 
#  guaranteedEngineManagerCPU: 4
#  guaranteedReplicaManagerCPU: 4
#  defaultLonghornStaticStorageClass: longhorn
#  backupTarget: nfs://${var.nfs_server}:/Multimedia
#resources:
#  limits:
#   cpu: 200m
#   memory: 200Mi
#  requests:
#   cpu: 100m
#   memory: 150Mi
#EOF
#  ]
#}
#
