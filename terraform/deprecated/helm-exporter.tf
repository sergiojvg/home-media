### DO NOT USE. OVERLOADS THE KUBERNETES API ##############################
###########################################################################
################################# HELM EXPORTER ###########################
###########################################################################
#resource "helm_release" "helm_exporter" {
#  name       = "helm-exporter"
#  chart      = "helm-exporter"
#  repository = "https://shanestarcher.com/helm-charts/"
#  namespace  = "default"
#  timeout    = "450"
#  version    = "1.2.3"
#
#  values = [<<EOF
#config:
#  helmRegistries:
#    override:
#    - allowAllReleases: true
#      charts:
#        - radarr
#        - sonarr
#        - homer
#        - tautulli
#      registry:
#        url: "https://k8s-at-home.com/charts/"
#    overrideChartNames: {}
#    registryNames:
#      - grafana
#      - k8s-at-home
#ingress:
#  annotations: {}
#  enabled: false
#  hosts:
#  - host: helm.${var.domain}
#    paths: []
#  tls: []
#EOF
#  ]
#}
