##########################################################################
############################### RTORRENT  ################################
##########################################################################
#
### Socket: /config/.local/share/rtorrent/rtorrent.sock
#
#tag: latest@sha256:b9a1032b38e6872f49d1e1a6d2c29624eb67092affcf5899b3a552fa76eb68b2
#resource "helm_release" "rtorrent" {
#  name       = "rtorrent"
#  chart      = "rtorrent-flood"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "120"
#  version    = var.versions.rtorrent.chart
#
#  values = [<<EOF
#image:
#  tag: ${var.versions.rtorrent.image}
#
#initContainers:
#  update-volume-permission:
#    image: busybox
#    command: ["sh", "-c", "chmod -R 777 /config"]
#    volumeMounts:
#    - name: config
#      mountPath: /config
#    securityContext:
#      runAsUser: 0
#
#priorityClassName: ${data.terraform_remote_state.infra.outputs.important_priority_class}
#
#ingress:
#  main:
#    enabled: true
#    annotations: 
#      kubernetes.io/ingress.class: nginx
#    hosts:
#    - host: rtorrent.${data.terraform_remote_state.infra.outputs.local_domain}
#      paths:
#        - path: /
#
#persistence:
#  config:
#    enabled: true
#    storageClass: ${data.terraform_remote_state.infra.outputs.longhorn_storage_class_name}
#    accessMode: ReadWriteOnce
#    size: 60Mi
#  downloads:
#    enabled: true
#    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
#    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
#    accessMode: "ReadWriteMany"
#    #mountPath: /downloads
#
#resources:
#  limits:
#    cpu: 600m
#    memory: 500Mi
#  requests:
#    cpu: 300m
#    memory: 300Mi
#
#config: |
#  session.use_lock.set = no
#  method.insert = cfg.basedir,  private|const|string, (cat,(fs.homedir),"/.local/share/rtorrent/")
#  method.insert = cfg.download, private|const|string, (cat,"/downloads/","download/")
#  method.insert = cfg.logs,     private|const|string, (cat,(cfg.download),"log/")
#  method.insert = cfg.logfile,  private|const|string, (cat,(cfg.logs),"rtorrent-",(system.time),".log")
#  method.insert = cfg.session,  private|const|string, (cat,(cfg.basedir),".session/")
#  method.insert = cfg.watch,    private|const|string, (cat,(cfg.download),"watch/")
#  fs.mkdir.recursive = (cat,(cfg.basedir))
#  fs.mkdir = (cat,(cfg.download))
#  fs.mkdir = (cat,(cfg.logs))
#  fs.mkdir = (cat,(cfg.session))
#  fs.mkdir = (cat,(cfg.watch))
#  fs.mkdir = (cat,(cfg.watch),"/load")
#  fs.mkdir = (cat,(cfg.watch),"/start")
#  schedule2 = watch_load, 11, 10, ((load.verbose, (cat, (cfg.watch), "load/*.torrent")))
#  schedule2 = watch_start, 10, 10, ((load.start_verbose, (cat, (cfg.watch), "start/*.torrent")))
#  dht = disable
#  dht.mode.set = disable
#  #dht.add_bootstrap = dht.transmissionbt.com:6881
#  #dht.add_bootstrap = dht.libtorrent.org:25401
#  throttle.max_uploads.set = 100
#  throttle.max_uploads.global.set = 250
#  throttle.min_peers.normal.set = 20
#  throttle.max_peers.normal.set = 60
#  throttle.min_peers.seed.set = 30
#  throttle.max_peers.seed.set = 80
#  throttle.global_up.max_rate.set_kb = 1
#  throttle.global_down.max_rate.set_kb = 4000
#  trackers.numwant.set = 80
#  network.port_range.set = 6881-6881
#  network.max_open_files.set = 600
#  network.max_open_sockets.set = 300
#  pieces.memory.max.set = 1800M
#  session.path.set = (cat, (cfg.session))
#  directory.default.set = (cat, (cfg.download))
#  log.execute = (cat, (cfg.logs), "execute.log")
#  encoding.add = utf8
#  system.daemon.set = true
#  system.umask.set = 0002
#  system.cwd.set = (directory.default)
#  network.http.max_open.set = 50
#  network.http.dns_cache_timeout.set = 25
#  network.scgi.open_local = (cat,(cfg.basedir),rtorrent.sock)
#  print = (cat, "Logging to ", (cfg.logfile))
#  log.open_file = "log", (cfg.logfile)
#  log.add_output = "info", "log"
#  # Enable the default ratio group.
#  ratio.enable=
#  # Change the limits, the defaults should be sufficient.
#  ratio.min.set=0
#  ratio.max.set=0
#  ratio.upload.set=1b
#  method.insert = d.get_finished_dir, simple, "cat=/downloads/,$d.custom1="
#  method.insert = d.data_path, simple, "if=(d.is_multi_file), (cat,(d.directory),/), (cat,(d.directory),/,(d.name))"
#  method.insert = d.move_to_complete, simple, "d.directory.set=$argument.1=; execute=mkdir,-p,$argument.1=; execute=mv,-u,$argument.0=,$argument.1=; d.save_full_session="
#  method.set_key = event.download.finished,move_complete,"d.move_to_complete=$d.data_path=,$d.get_finished_dir="
#  max_peers_seed = 0
#  system.file.allocate.set = 2
#EOF
#  ]
#}

#resource "kubernetes_ingress_v1" "rtorrent" {
#  metadata {
#    name = "rtorrent"
#    annotations = {
#      "kubernetes.io/ingress.class" = "traefik"
#      "cert-manager.io/cluster-issuer" : kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
#    }
#  }
#  spec {
#    tls {
#      secret_name = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
#    }
#    rule {
#      host = "rtorrent.${var.domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "rtorrent-rtorrent-flood"
#              port {
#                number = 3000
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#    rule {
#      host = "rtorrent.${var.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "rtorrent-rtorrent-flood"
#              port {
#                number = 3000
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.rtorrent]
#}
