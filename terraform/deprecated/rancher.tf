#########################################################################
################################# RANCHER ###############################
#########################################################################
#resource "kubernetes_namespace" "rancher" {
#  metadata {
#    name = "cattle-system"
#  }
#}
#
#resource "helm_release" "rancher" {
#  name       = "rancher"
#  chart      = "rancher"
#  repository = "https://releases.rancher.com/server-charts/latest"
#  namespace  = kubernetes_namespace.rancher.metadata.0.name
#  timeout    = "300"
#  version    = "2.7.0"
#
#  values = [<<EOF
#hostname: rancher.my.house
#replicas: 1
#bootstrapPassword: password
#EOF
#  ]
#  depends_on = [kubernetes_persistent_volume_claim.nfs-pvc]
#}
#
##resource "kubernetes_ingress_v1" "plex" {
##  metadata {
##    name = "plex"
##    annotations = {
##      "kubernetes.io/ingress.class" = "traefik"
##      "cert-manager.io/cluster-issuer" : kubernetes_manifest.clusterissuer.manifest.metadata.name
##    }
##  }
##  spec {
##    tls {
##      #secret_name = "ha-my-house.duckdns.org-tls"
##      secret_name = kubernetes_manifest.plex_certificate.manifest.spec.secretName
##    }
##    rule {
##      host = "plex.${var.domain}"
##      http {
##        path {
##          backend {
##            service {
##              name = "plex"
##              port {
##                number = 32400
##              }
##            }
##          }
##          path = "/"
##        }
##      }
##    }
##    rule {
##      host = "plex-my-house.${var.public_domain}"
##      http {
##        path {
##          backend {
##            service {
##              name = "plex"
##              port {
##                number = 32400
##              }
##            }
##          }
##          path = "/"
##        }
##      }
##    }
##  }
##  depends_on = [helm_release.plex]
##}
#
