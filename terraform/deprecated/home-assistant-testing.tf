##########################################################################
############################ HOME ASSISTANT TESTING ##############################
##########################################################################
#
#resource "kubernetes_persistent_volume_v1" "homeassistant_configs_testing" {
#  metadata {
#    name = "homeassistant-configs-testing"
#  }
#  spec {
#    capacity = {
#      #storage = var.media_size
#      storage = "20Gi"
#    }
#    access_modes       = ["ReadWriteOnce"]
#    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata.0.name
#    persistent_volume_source {
#      iscsi {
#        fs_type       = "ext4"
#        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.hatest.c346ec"
#        target_portal = "192.168.1.205"
#        #lun = 0
#      }
#    }
#  }
#  timeouts {
#    create = "1m"
#  }
#}
#
#resource "kubernetes_persistent_volume_claim_v1" "homeassistant_configs_testing" {
#  metadata {
#    name = "homeassistant-config-testing"
#  }
#  spec {
#    volume_name        = kubernetes_persistent_volume_v1.homeassistant_configs_testing.metadata.0.name
#    storage_class_name = kubernetes_storage_class.nfs_dummy.metadata.0.name
#    access_modes       = ["ReadWriteOnce"]
#    resources {
#      requests = {
#        storage = "20Gi"
#      }
#    }
#  }
#  timeouts {
#    create = "1m"
#  }
#}
#
#
#resource "helm_release" "homeassistant_testing" {
#  name       = "homeassistant-testing"
#  chart      = "home-assistant"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "300"
#  version    = var.versions.homeassistant.chart
#
#  values = [<<EOF
#image:
#  #repository: homeassistant/home-assistant
#  tag: ${var.versions.homeassistant.image}
##hostNetwork: true
#dnsPolicy: ClusterFirstWithHostNet
##securityContext:
##  privileged:  true
##priorityClassName: ${kubernetes_priority_class_v1.important.metadata.0.name}
#persistence:
#  config:
#    enabled: true
#    storageClass: ${kubernetes_storage_class.nfs_dummy.metadata.0.name}
#    existingClaim: ${kubernetes_persistent_volume_claim_v1.homeassistant_configs_testing.metadata.0.name}
#    accessMode: ReadWriteOnce
#    size: 20Gi
#resources:
#  limits:
#    cpu: 500m
#    memory: 500Mi
#  requests:
#    cpu: 300m
#    memory: 300Mi
#addons:
#  codeserver:
#    enabled: true
#    args:
#    - --auth
#    - none
#    - --user-data-dir
#    - "/config/.vscode"
#
#    workingDir: "/config"
#
#    volumeMounts:
#    - name: config
#      mountPath: /config
#EOF
#  ]
#}
#
#resource "kubernetes_ingress_v1" "homeassistant_home_testing" {
#  metadata {
#    name = "homeassistant-home-testing"
#    annotations = {
#      "kubernetes.io/ingress.class"  = "nginx"
#      "nginx.org/websocket-services" = "homeassistant-testing-home-assistant"
#    }
#  }
#  spec {
#    rule {
#      host = "test-ha.${var.local_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "homeassistant-testing-home-assistant"
#              port {
#                number = 8123
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.homeassistant_testing]
#}
#
#resource "kubernetes_ingress_v1" "homeassistant_external_testing" {
#  metadata {
#    name = "homeassistant-external-testing"
#    annotations = {
#      "kubernetes.io/ingress.class"    = "nginx"
#      "cert-manager.io/cluster-issuer" = kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
#      "nginx.org/redirect-to-https"    = true
#      "nginx.org/websocket-services"   = "homeassistant-testing-home-assistant"
#    }
#  }
#  spec {
#    tls {
#      secret_name = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
#      hosts       = ["test-homeassistant.${var.public_domain}"]
#    }
#    rule {
#      host = "test-homeassistant.${var.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "homeassistant-testing-home-assistant"
#              port {
#                number = 8123
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.homeassistant_testing]
#}
#
#resource "kubernetes_ingress_v1" "homeassistant-vscode-testing" {
#  metadata {
#    name = "homeassistant-vscode-testing"
#    annotations = {
#      "kubernetes.io/ingress.class" = "nginx"
#    }
#  }
#  spec {
#    rule {
#      host = "testcode.ha.${var.local_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "homeassistant-testing-home-assistant-codeserver"
#              port {
#                number = 12321
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.homeassistant_testing]
#}