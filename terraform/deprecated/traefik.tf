resource "kubernetes_secret" "traefik_basic_auth" {
  metadata {
    name = "traefik-basic-auth"
  }

  data = {
    username = data.sops_file.secrets.data["traefik_basic_auth_user"]
    password = data.sops_file.secrets.data["traefik_basic_auth_password"]
  }

  type = "kubernetes.io/basic-auth"
}

resource "kubernetes_manifest" "traefik_basic_auth" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "Middleware"
    "metadata" = {
      "name"      = "basic-auth"
      "namespace" = "default"
    }
    "spec" = {
      "basicAuth" = {
        "secret" = kubernetes_secret.traefik_basic_auth.metadata.0.name
      }
    }
  }
}
