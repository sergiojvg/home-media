##########################################################################
################################## GROCY #################################
##########################################################################
#resource "helm_release" "grocy" {
#  name       = "grocy"
#  chart      = "grocy"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "300"
#  version    = "8.1.0"
#
#  values =[<<EOF
##initContainers:
##  copy-configmap:
##    name: copy-configmap
##    image: busybox
##    command:
##    - "sh"
##    - "-c"
##    - |
##      echo "--> Retrieve DB"
##      wget https://github.com/sergiojvg/home-media-configs/raw/main/OmbiSettings.db -O /opt/config/OmbiSettings.db
##      chmod -R 777 /opt/config/OmbiSettings.db
##    volumeMounts:
##    - name: config
##      mountPath: /opt/config
##    securityContext:
##      runAsUser: 0
#ingress:
#  main:
#    enabled: true
#    hosts:
#      - host: grocy.my.house
#        paths:
#          - path: /
#resources:
#  limits:
#    cpu: 600m
#    memory: 350Mi
#  requests:
#    cpu: 500m
#    memory: 300Mi
#persistence:
#  config:
#    enabled: true
#    storageClass: ${local.sc_name}
#    accessMode: ReadWriteMany
#EOF
# ]
#  depends_on = [helm_release.sonarr]
#}
#
##resource "kubernetes_ingress" "groci" {
##  metadata {
##    name = "groci"
##  }
##  spec {
##    rule {
##      host = "ombi.${var.domain}"
##      http {
##        path {
##          backend {
##            service_name = "ombi"
##            service_port = 3579
##          }
##          path = "/"
##        }
##      }
##    }
##  }
##  depends_on = [helm_release.ombi]
##}
##
##data "kubernetes_ingress" "ombi" {
##  metadata {
##    name = "ombi"
##  }
##  depends_on = [kubernetes_ingress.ombi]
##}
