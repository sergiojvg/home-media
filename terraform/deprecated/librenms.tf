#########################################################################
################################# LIBRENMS ##############################
#########################################################################

### This requires a MariaDB instance

resource "kubernetes_deployment_v1" "librenms" {
  metadata {
    name = "librenms"
    labels = {
      app = "librenms"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "librenms"
      }
    }
    template {
      metadata {
        labels = {
          app = "librenms"
        }
      }
      spec {
        host_network = true
        container {
          image = "librenms/librenms:24.8.1"
          name  = "librenms"
          #command = ["/bin/bash", "-c"]
          #args    = ["cp /temp-config/settings.json /config/settings.json && /init"]

          resources {
            limits = {
              cpu    = "300m"
              memory = "400Mi"
            }
            requests = {
              cpu    = "200m"
              memory = "400Mi"
            }
          }
          security_context {
            privileged = true
          }
          liveness_probe {
            tcp_socket {
              port = 8000
            }
          }
          readiness_probe {
            tcp_socket {
              port = 8000
            }
          }
        }
      }
    }
  }
  timeouts {
    create = "2m"
  }
}

resource "kubernetes_service" "librenms" {
  metadata {
    name = "librenms"
  }
  spec {
    selector = {
      app = "librenms"
    }
    port {
      port        = 8000
      target_port = 8000
    }
  }
  timeouts {
    create = "1m"
  }
}
