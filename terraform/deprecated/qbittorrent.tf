###########################################################################
################################ qbittorrent ##############################
###########################################################################
##
#resource "helm_release" "qbittorrent" {
#  name       = "qbittorrent"
#  chart      = "qbittorrent"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "120"
#  #version    = var.versions.rtorrent.chart
#  version    = "13.5.2"
#
#  values = [<<EOF
#
##initContainers:
##  update-volume-permission:
##    image: busybox
##    command: ["sh", "-c", "chmod -R 777 /config"]
##    volumeMounts:
##    - name: config
##      mountPath: /config
##    securityContext:
##      runAsUser: 0
#
#ingress:
#  main:
#    enabled: true
#    hosts:
#    - host: qbittorrent.${data.terraform_remote_state.infra.outputs.local_domain}
#      paths:
#        - path: /
#
#persistence:
#  config:
#    enabled: true
#    storageClass: ${data.terraform_remote_state.infra.outputs.longhorn_storage_class_name}
#    accessMode: ReadWriteOnce
#    size: 60Mi
#  downloads:
#    enabled: true
#    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
#    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
#    accessMode: "ReadWriteMany"
#    #mountPath: /downloads
#
#resources:
#  limits:
#    cpu: 600m
#    memory: 500Mi
#  requests:
#    cpu: 300m
#    memory: 300Mi
#
#EOF
#  ]
#}
#
##resource "kubernetes_ingress_v1" "rtorrent" {
##  metadata {
##    name = "rtorrent"
##    annotations = {
##      "kubernetes.io/ingress.class" = "traefik"
##      "cert-manager.io/cluster-issuer" : kubernetes_manifest.dns_clusterissuer.manifest.metadata.name
##    }
##  }
##  spec {
##    tls {
##      secret_name = kubernetes_manifest.wildcard_certificate.manifest.spec.secretName
##    }
##    rule {
##      host = "rtorrent.${var.domain}"
##      http {
##        path {
##          backend {
##            service {
##              name = "rtorrent-rtorrent-flood"
##              port {
##                number = 3000
##              }
##            }
##          }
##          path = "/"
##        }
##      }
##    }
##    rule {
##      host = "rtorrent.${var.public_domain}"
##      http {
##        path {
##          backend {
##            service {
##              name = "rtorrent-rtorrent-flood"
##              port {
##                number = 3000
##              }
##            }
##          }
##          path = "/"
##        }
##      }
##    }
##  }
##  depends_on = [helm_release.rtorrent]
##}
#
