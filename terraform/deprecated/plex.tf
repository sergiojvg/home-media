#########################################################################
################################# PLEX ##################################
#########################################################################
#resource "helm_release" "plex" {
#  name       = "plex"
#  chart      = "plex"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "100"
#  version    = var.versions.plex.chart
#
#  values = [<<EOF
#image:
#  repository: linuxserver/plex
#  tag: ${var.versions.plex.image}
#claimToken: ${data.sops_file.secrets.data["plex_token"]}
#timezone: ${data.terraform_remote_state.infra.outputs.timezone}
#env:
#  TZ: ${data.terraform_remote_state.infra.outputs.timezone}
#  PLEX_CLAIM: ${data.sops_file.secrets.data["plex_token"]}
#ingress:
#  main:
#    enabled: false
#resources:
#  limits:
#    cpu: 2000m
#    memory: 1024Mi
#  requests:
#    cpu: 1500m
#    memory: 900Mi
#hostNetwork: true
#persistence:
#  media:
#    enabled: true
#    emptyDir: false
#    mountPath: /downloads
#    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
#    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
#    accessMode: ReadWriteMany
#  config:
#    enabled: true
#    mountPath: /config
#    storageClass: ${data.terraform_remote_state.infra.outputs.longhorn_storage_class_name}
#    accessMode: "ReadWriteOnce"
#    size: 10Gi
#EOF
#  ]
#}
#
#resource "kubernetes_ingress_v1" "plex_home" {
#  metadata {
#    name = "plex-home"
#    annotations = {
#      "kubernetes.io/ingress.class" = "traefik"
#    }
#  }
#  spec {
#    rule {
#      host = "plex.${data.terraform_remote_state.infra.outputs.local_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "plex"
#              port {
#                number = 32400
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#}
#
#resource "kubernetes_ingress_v1" "plex_external" {
#  metadata {
#    name = "plex-external"
#    annotations = {
#      "kubernetes.io/ingress.class"                      = "traefik"
#      "cert-manager.io/cluster-issuer"                   = data.terraform_remote_state.infra.outputs.certmanager_clusterissuer_name
#      "traefik.ingress.kubernetes.io/router.tls"         = "true"
#      "traefik.ingress.kubernetes.io/router.entrypoints" = "websecure"
#    }
#  }
#  spec {
#    tls {
#      secret_name = data.terraform_remote_state.infra.outputs.wildcard_ssl_certificate_secret_name
#      hosts       = ["*.${data.terraform_remote_state.infra.outputs.public_domain}"]
#    }
#    rule {
#      host = "plex.${data.terraform_remote_state.infra.outputs.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "plex"
#              port {
#                number = 32400
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.plex]
#}
#
#resource "kubernetes_ingress_v1" "plex_external_http_redirect" {
#  metadata {
#    name = "plex-external-http-redirect"
#    annotations = {
#      "kubernetes.io/ingress.class"                      = "traefik"
#      "cert-manager.io/cluster-issuer"                   = data.terraform_remote_state.infra.outputs.certmanager_clusterissuer_name
#      "traefik.ingress.kubernetes.io/router.middlewares" = data.terraform_remote_state.infra.outputs.traefik_redirect_middleware_name
#      "traefik.ingress.kubernetes.io/router.entrypoints" = "web"
#    }
#  }
#  spec {
#    rule {
#      host = "plex.${data.terraform_remote_state.infra.outputs.public_domain}"
#      http {
#        path {
#          backend {
#            service {
#              name = "plex"
#              port {
#                number = 32400
#              }
#            }
#          }
#          path = "/"
#        }
#      }
#    }
#  }
#  depends_on = [helm_release.plex]
#}
