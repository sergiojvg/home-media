###########################################################################
################################## tdarr #################################
###########################################################################
#resource "kubernetes_persistent_volume_v1" "tdarr_configs" {
#  metadata {
#    name = "tdarr-configs"
#  }
#  spec {
#    capacity = {
#      #storage = var.media_size
#      storage = "5Gi"
#    }
#    access_modes       = ["ReadWriteOnce"]
#    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
#    persistent_volume_source {
#      iscsi {
#        fs_type       = "ext4"
#        iqn           = "iqn.2004-04.com.qnap:ts-219pplus:iscsi.tdarr.c346ec"
#        target_portal = "192.168.1.205"
#        #lun = 0
#      }
#    }
#  }
#  timeouts {
#    create = "1m"
#  }
#}
#
#resource "kubernetes_persistent_volume_claim_v1" "tdarr_configs" {
#  metadata {
#    name = "tdarr-config"
#  }
#  spec {
#    volume_name        = kubernetes_persistent_volume_v1.tdarr_configs.metadata.0.name
#    storage_class_name = data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class
#    access_modes       = ["ReadWriteOnce"]
#    resources {
#      requests = {
#        storage = "5Gi"
#      }
#    }
#  }
#  timeouts {
#    create = "1m"
#  }
#}
#
#resource "helm_release" "tdarr" {
#  name       = "tdarr"
#  chart      = "tdarr"
#  repository = "https://k8s-at-home.com/charts/"
#  namespace  = "default"
#  timeout    = "100"
#  version    = var.versions.tdarr.chart
#
#  values = [<<EOF
#image:
#  #repository: linuxserver/tdarr
#  tag: ${var.versions.tdarr.image}
#node:
#  enabled: true
#  id: node
#  image:
#    #repository: haveagitgat/tdarr_node
#    tag: ${var.versions.tdarr.image}
#  resources: 
#  limits:
#    cpu: 400m
#    memory: 350Mi
#  requests:
#    cpu: 200m
#    memory: 200Mi
#resources:
#  limits:
#    cpu: 400m
#    memory: 350Mi
#  requests:
#    cpu: 200m
#    memory: 200Mi
##priorityClassName: ${data.terraform_remote_state.infra.outputs.important_priority_class}
#ingress:
#  main:
#    enabled: true
#    annotations: 
#      kubernetes.io/ingress.class: nginx
#    hosts:
#      - host: tdarr.${data.terraform_remote_state.infra.outputs.local_domain}
#        paths:
#          - path: /
#persistence:
#  config:
#    enabled: true
#    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
#    existingClaim: ${kubernetes_persistent_volume_claim_v1.tdarr_configs.metadata.0.name}
#    accessMode: ReadWriteOnce
#    size: 5Gi
#  media:
#    enabled: true
#    emptyDir: false
#    mountPath: /media
#    storageClass: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_storage_class}
#    existingClaim: ${data.terraform_remote_state.infra.outputs.nfs_multimedia_pvc}
#    accessMode: ReadWriteMany
#EOF
#  ]
#}
#
