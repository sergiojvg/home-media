#resource "zerotier_network" "b_e_n" {
#  name        = "B.E.N"
#  description = "Managed by Terraform"
#  #assign_ipv4 = {
#  #  zerotier = true
#  #}
#  assignment_pool {
#    start = "10.1.0.1"
#    end   = "10.1.0.254"
#  }
#  route {
#    target = "10.1.0.0/24"
#  }
#  #dynamic "route" {
#  #  for_each = var.kubernetes_zerotier_address
#  #  content {
#  #    target = "192.168.2.19${sum([route.key, 1])}/32"
#  #    via    = "10.1.0.19${sum([route.key, 1])}"
#  #  }
#  #}
#  route {
#    target = "192.168.1.0/24"
#    via    = "10.1.0.201"
#  }
#  enable_broadcast = true
#  private          = false
#  flow_rules       = "accept;"
#}

#resource "zerotier_member" "cluster_nodes" {
#  count                   = length(var.kubernetes_zerotier_address)
#  name                    = "node-${sum([count.index, 1])}"
#  member_id               = var.kubernetes_zerotier_address[count.index]
#  network_id              = zerotier_network.b_e_n.id
#  description             = "Kubernetes Node ${sum([count.index, 1])}"
#  hidden                  = false
#  allow_ethernet_bridging = true
#  no_auto_assign_ips      = true
#  ip_assignments          = ["10.1.0.20${sum([count.index, 1])}"]
#}
