#########################################################################
########################## PROMETHEUS SNMP EXPORTER #####################
#########################################################################

#resource "kubernetes_config_map" "snmp_qnap" {
#  metadata {
#    name = "snmp-qnap"
#  }
#
#  data = {
#    "snmp.yaml" = <<EOF
## WARNING: This file was auto-generated using snmp_exporter generator, manual changes will be lost.
#qnap:
#  walk:
#  - 1.3.6.1.4.1.24681.1.3.15.1.3
#  - 1.3.6.1.4.1.24681.1.3.17.1.5
#  - 1.3.6.1.4.1.24681.1.3.17.1.6
#  - 1.3.6.1.4.1.24681.1.3.9.1.3
#  - 1.3.6.1.4.1.24681.1.3.9.1.4
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.1.1.2.1.7
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.1.4.3.1.3
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.5
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.6
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.5
#  - 1.3.6.1.4.1.24681.1.4.1.11.5.6.2.1.3
#  - 1.3.6.1.4.1.24681.1.4.1.11.5.6.2.1.4
#  get:
#  - 1.3.6.1.4.1.24681.1.2.3.0
#  - 1.3.6.1.4.1.24681.1.3.1.0
#  - 1.3.6.1.4.1.24681.1.3.3.0
#  - 1.3.6.1.4.1.24681.1.3.4.0
#  - 1.3.6.1.4.1.24681.1.3.6.0
#  metrics:
#  - name: systemFreeMem
#    oid: 1.3.6.1.4.1.24681.1.2.3
#    type: DisplayString
#    help: System free memory - 1.3.6.1.4.1.24681.1.2.3
#  - name: systemCPU_UsageEX
#    oid: 1.3.6.1.4.1.24681.1.3.1
#    type: gauge
#    help: system CPU usage - 1.3.6.1.4.1.24681.1.3.1
#  - name: sysFanSpeedEX
#    oid: 1.3.6.1.4.1.24681.1.3.15.1.3
#    type: gauge
#    help: System fan speed (RPM). - 1.3.6.1.4.1.24681.1.3.15.1.3
#    indexes:
#    - labelname: sysFanIndexEX
#      type: gauge
#  - name: sysVolumeFreeSizeEX
#    oid: 1.3.6.1.4.1.24681.1.3.17.1.5
#    type: counter
#    help: System Volume free size in byte. - 1.3.6.1.4.1.24681.1.3.17.1.5
#    indexes:
#    - labelname: sysVolumeIndexEX
#      type: gauge
#  - name: sysVolumeStatusEX
#    oid: 1.3.6.1.4.1.24681.1.3.17.1.6
#    type: DisplayString
#    help: System Volume status. - 1.3.6.1.4.1.24681.1.3.17.1.6
#    indexes:
#    - labelname: sysVolumeIndexEX
#      type: gauge
#  - name: systemFreeMemEX
#    oid: 1.3.6.1.4.1.24681.1.3.3
#    type: counter
#    help: System free memory in byte - 1.3.6.1.4.1.24681.1.3.3
#  - name: systemUptimeEX
#    oid: 1.3.6.1.4.1.24681.1.3.4
#    type: gauge
#    help: The amount of time since this host was last initialized - 1.3.6.1.4.1.24681.1.3.4
#  - name: systemTemperatureEX
#    oid: 1.3.6.1.4.1.24681.1.3.6
#    type: gauge
#    help: System temperature in centigrade - 1.3.6.1.4.1.24681.1.3.6
#  - name: ifPacketsReceivedEX
#    oid: 1.3.6.1.4.1.24681.1.3.9.1.3
#    type: counter
#    help: System packets received. - 1.3.6.1.4.1.24681.1.3.9.1.3
#    indexes:
#    - labelname: ifIndexEX
#      type: gauge
#  - name: ifPacketsSentEX
#    oid: 1.3.6.1.4.1.24681.1.3.9.1.4
#    type: counter
#    help: System packets sent. - 1.3.6.1.4.1.24681.1.3.9.1.4
#    indexes:
#    - labelname: ifIndexEX
#      type: gauge
#  - name: enclosureSystemTemp
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.1.1.2.1.7
#    type: gauge
#    help: Enclosure System temperature in centigrade. - 1.3.6.1.4.1.24681.1.4.1.1.1.1.1.2.1.7
#    indexes:
#    - labelname: enclosureIndex
#      type: gauge
#  - name: cpuUsage
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.1.4.3.1.3
#    type: gauge
#    help: CPUUsage. - 1.3.6.1.4.1.24681.1.4.1.1.1.1.4.3.1.3
#    indexes:
#    - labelname: cpuIndex
#      type: gauge
#  - name: diskSmartInfo
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.5
#    type: gauge
#    help: DiskSmartInfo. - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.5
#    indexes:
#    - labelname: diskIndex
#      type: gauge
#    enum_values:
#      -1: error
#      0: good
#      1: warning
#      2: abnormal
#  - name: diskTemperture
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.6
#    type: gauge
#    help: DiskTemperture. - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.6
#    indexes:
#    - labelname: diskIndex
#      type: gauge
#  - name: volumeStatus
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.5
#    type: DisplayString
#    help: Volume status - 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.5
#    indexes:
#    - labelname: volumeIndex
#      type: gauge
#  - name: iops
#    oid: 1.3.6.1.4.1.24681.1.4.1.11.5.6.2.1.3
#    type: gauge
#    help: IOPS. - 1.3.6.1.4.1.24681.1.4.1.11.5.6.2.1.3
#    indexes:
#    - labelname: diskPerformanceIndex
#      type: gauge
#  - name: latency
#    oid: 1.3.6.1.4.1.24681.1.4.1.11.5.6.2.1.4
#    type: gauge
#    help: Latency. - 1.3.6.1.4.1.24681.1.4.1.11.5.6.2.1.4
#    indexes:
#    - labelname: diskPerformanceIndex
#      type: gauge
#qnaplong:
#  walk:
#  - 1.3.6.1.4.1.24681.1.3.17.1.2
#  - 1.3.6.1.4.1.24681.1.3.17.1.4
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.8
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.9
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.3
#  - 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.8
#  get:
#  - 1.3.6.1.4.1.24681.1.2.2.0
#  - 1.3.6.1.4.1.24681.1.3.12.0
#  - 1.3.6.1.4.1.24681.1.3.13.0
#  - 1.3.6.1.4.1.24681.1.3.2.0
#  metrics:
#  - name: systemTotalMem
#    oid: 1.3.6.1.4.1.24681.1.2.2
#    type: DisplayString
#    help: System total memory - 1.3.6.1.4.1.24681.1.2.2
#  - name: modelNameEX
#    oid: 1.3.6.1.4.1.24681.1.3.12
#    type: DisplayString
#    help: Model name - 1.3.6.1.4.1.24681.1.3.12
#  - name: hostNameEX
#    oid: 1.3.6.1.4.1.24681.1.3.13
#    type: DisplayString
#    help: Model name - 1.3.6.1.4.1.24681.1.3.13
#  - name: sysVolumeDescrEX
#    oid: 1.3.6.1.4.1.24681.1.3.17.1.2
#    type: DisplayString
#    help: A textual string containing information about the volume. - 1.3.6.1.4.1.24681.1.3.17.1.2
#    indexes:
#    - labelname: sysVolumeIndexEX
#      type: gauge
#  - name: sysVolumeTotalSizeEX
#    oid: 1.3.6.1.4.1.24681.1.3.17.1.4
#    type: counter
#    help: System Volume total size in byte. - 1.3.6.1.4.1.24681.1.3.17.1.4
#    indexes:
#    - labelname: sysVolumeIndexEX
#      type: gauge
#  - name: systemTotalMemEX
#    oid: 1.3.6.1.4.1.24681.1.3.2
#    type: counter
#    help: System total memory in byte - 1.3.6.1.4.1.24681.1.3.2
#  - name: diskModel
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.8
#    type: DisplayString
#    help: DiskModel. - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.8
#    indexes:
#    - labelname: diskIndex
#      type: gauge
#  - name: diskCapacity
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.9
#    type: counter
#    help: DiskCapacity. - 1.3.6.1.4.1.24681.1.4.1.1.1.1.5.2.1.9
#    indexes:
#    - labelname: diskIndex
#      type: gauge
#  - name: volumeCapacity
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.3
#    type: counter
#    help: Volume capacity in byte. - 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.3
#    indexes:
#    - labelname: volumeIndex
#      type: gauge
#  - name: volumeName
#    oid: 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.8
#    type: DisplayString
#    help: Volume alias name. - 1.3.6.1.4.1.24681.1.4.1.1.1.2.3.2.1.8
#    indexes:
#    - labelname: volumeIndex
#      type: gauge
#EOF
#  }
#}
#
#resource "helm_release" "prometheus_snmp_exporter" {
#  name       = "prometheus-snmp-exporter"
#  chart      = "prometheus-snmp-exporter"
#  repository = "https://prometheus-community.github.io/helm-charts"
#  namespace  = "default"
#  timeout    = "300"
#  version    = "1.2.1"
#
#  values = [<<EOF
## config:
#resources:
#  limits:
#    cpu: 50m
#    memory: 60Mi
#  requests:
#    cpu: 50m
#    memory: 60Mi
#extraConfigmapMounts:
#   - name: snmp-exporter-configmap
#     mountPath: /run/secrets/snmp-exporter
#     subPath: snmp.yaml # (optional)
#     configMap: snmp-qnap
#     readOnly: true
#     defaultMode: 420
#ingress:
#  enabled: false
#  hosts:
#    - host: snmp.my.house
#      paths:
#        - path: /
#          pathType: ImplementationSpecific
#EOF
#  ]
#}
